// Initializing and Configuring Kendo UI GRID

var GridData;
$(document).ready(function() {

	var dataSource = new kendo.data.DataSource({
		type : "json",
		transport : {
			read : "data.json"
		},
		pageSize : 20

	});
	GridData = $("#grid").kendoGrid({
		dataSource : dataSource,
		height : 550,
		sortable : true,
		toolbar : '<div class="toolbar"><label class="category-label" for="nameSearch">Name:</label><input class=k-textbox type=text id="nameSearch" ng-model="search.Name" ng-change="searchMe(search)" placeholder="Search by Name..." /> 	<label class="category-label" id="categoryLabel" for="category">Department:</label><input type="search" id="category" ng-model="search.Department" ng-change="searchMe(search)" style="width: 150px"/></div>',
		pageable : {
			refresh : true,
			pageSizes : true,
			buttonCount : 5
		},
		filterable : false,
		columns : [{
			field : "Code",
			title : "Code"
			
		}, {
			field : "Name",
			title : "Name"
		}, {
			field : "Department",
			title : "Department"
		}, {
			field : "HiringDate",
			title : "Hiring Date",

		}, {
			field : "Active",
			title : "Active"
		}],
		rowTemplate : kendo.template($("#rowTemplate").html())
	});

	//Getting Data(categories) for Grid toolbar Department List

	var dropDown = GridData.find("#category").kendoDropDownList({
		dataTextField : "Department",
		dataValueField : "Department",
		autoBind : false,
		optionLabel : "All",
		dataSource : {
			type : "json",
			transport : {
				read : "dataCat.json"
			}

		}
	});

});

//END
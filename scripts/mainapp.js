//Main Angularjs Application Code

var app = angular.module("Dashboard", ['ngRoute', 'ui.bootstrap']);




//Routing
app.config(function($routeProvider, $locationProvider) {
	$routeProvider.when('/', {
		templateUrl : 'Views/home.html',
		controller : 'home'
	}).when('/employee', {
		templateUrl : 'Views/employee.html',
		controller : 'employee'
	}).when('/addemployee', {
		templateUrl : 'Views/addemployee.html',
		controller : 'addemployee'
	}).otherwise({
		redirectTo : '/'
	});

});




//I have created a service to share the data between all controllers
app.factory('activeLink', function() {
	return {
		num : 0
	};
});




//Common Controller(Master)

//I have defined the controller that way to be minifing friendly
app.controller("mainController", ['$scope', 'activeLink', '$sce',
function($scope, activeLink, $sce) {

	$scope.navbarAppear = true;


	//getting reference to Service object
	$scope.activeLink = activeLink;


	//Top nav Time label code
	$scope.timeUpdate = function() {
		var now = new Date();
		// current date

		var time = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();

		// a cleaner way than string concatenation
		var date = [now.getDate(), now.getMonth() + 1, now.getFullYear()].join('/');

		// setting the content of the element with the ID time to the formatted string
		$scope.dateTimeLabel = $sce.trustAsHtml([date, time].join(' &nbsp; '));
		$scope.$apply();

		// calling this function again in 1000ms
		setTimeout($scope.timeUpdate, 1000);
	};

	// initial call
	$scope.timeUpdate();

}]);




// Home (dashboard) Controller
app.controller("home", ['$scope', 'activeLink',
function($scope, activeLink) {

	activeLink.num = 1;

}]);




// Employee Data Controller
app.controller("employee", ['$scope', 'activeLink',
function($scope, activeLink) {

	activeLink.num = 2;


	//Searching Grid for data based on search input and Department droplist
	//	with default ""(to get all) when value is undefined or empty
	$scope.searchMe = function(employee) {

		GridData.data("kendoGrid").dataSource.filter({
			logic : "and",
			filters : [{
				field : "Name",
				operator : "contains",
				value : employee.Name || ""
			}, {
				field : "Department",
				operator : "contains",
				value : employee.Department || ""
			}]
		});
	};

}]);




//Add Employee Controller
app.controller("addemployee", ['$scope', 'activeLink',
function($scope, activeLink) {

	activeLink.num = 3;

	$scope.employee = {};
	$scope.employee.active = true;


	//Bug on the calendar when u choose a date it's model is before that date by 2 hours
	//	fixed by adding 2 hours to it
	$scope.$watch('employee.startDate', function(e) {
		$scope.fixMyTime(e);
	});
	$scope.$watch('employee.HireDate', function(e) {
		$scope.fixMyTime(e);
	});

	$scope.fixMyTime = function(e) {

		if (e.getHours() == 0) {
			e.setHours(e.getHours() + 2);
		}
	};


	// Disable weekend selection
	$scope.disabled = function(date, mode) {
		return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6 ) );
	};


	//Toggle opening different calendars
	//	by dynamically calling properties ['']
	$scope.open = function($event, num) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope['opened' + num] = !$scope['opened' + num];
	};


	//Calendar Formating
	$scope.dateOptions = {
		formatYear : 'yy',
		startingDay : 1
	};

	$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'dd/MM/yyyy', 'shortDate'];
	$scope.format = $scope.formats[3];


	//Saving employee(validation and alertify success )
	$scope.save = function() {
		$scope.$broadcast('show-errors-check-validity');

		if ($scope.myForm.$valid) {
			alertify.success('<i style="font-size:18px" class="fa fa-check"></i> Employee Saved successfully');
			$scope.reset();
		}
	};


	//Reset employee object to default and clear errors
	$scope.reset = function() {
		$scope.$broadcast('show-errors-reset');
		$scope.employee = {
			active : true
		};
	};
}]);

